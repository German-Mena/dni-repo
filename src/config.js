require('./db/dbConfig')
const { app, BrowserWindow, Menu, ipcMain } = require('electron');
const url = require('url');
const path = require('path');

let NuevaVisitaDB = require('./db/dbModels');

let mainWindow, newProductWindow;

app.on('ready', () => {

    mainWindow = new BrowserWindow({
        //Esto lo hago para poder requerir paquetes en el Script del index.html
        webPreferences: {
            nodeIntegration: true
        }
    })

    mainWindow.loadURL(url.format({
        pathname: path.join(__dirname, 'views/index.html'),
        protocol: 'file',
        slashes: true
    }))

    const mainMenu = Menu.buildFromTemplate(templateMenu)
    Menu.setApplicationMenu(mainMenu)

    mainWindow.on('closed', () => {
        app.quit()
    })

})

ipcMain.on('nuevaVisita', (event, dni) => {

    array = dni.split('@')
    dni = {
        nombre: array[2],
        apellido: array[1],
        dni: array[4]
    }

    guardarVisita(dni);

    console.log('Nueva visita: ', dni);
    newProductWindow.close();
    mainWindow.webContents.send('nuevaVisita', dni);
})

const templateMenu = [
    {
        label: 'File',
        submenu: [
            {
                label: 'Nueva visita',
                acelerator: 'Ctrl+N',
                click() {
                    createNewProductWindow()
                }
            }
        ]
    },
    {
        label: 'Remove all Products',
        click() {
            mainWindow.webContents.send('product: remove-all')
        }
    },
    {
        label: 'Exit',
        //De esta forma evaluo cual es el sistema operativo en el que esta la App
        acelerator: process.platform == 'darwin' ? 'command+Q' : 'Ctrl+Q',
        click() {
            app.quit()
        }
    }
]

function createNewProductWindow() {

    newProductWindow = new BrowserWindow({
        width: 400,
        height: 200,
        title: 'Add a new Product',

        //Esto lo hago para poder requerir paquetes en el Script del new-product.html
        webPreferences: {
            nodeIntegration: true
        }
    })

    //newProductWindow.setMenu(null)

    newProductWindow.loadURL(url.format({
        pathname: path.join(__dirname, 'views/nuevaVisita.html'),
        protocol: 'file',
        slashes: true
    }))

    newProductWindow.on('closed', () => {
        newProductWindow = null
    })
}

if (process.env.NODE_ENV !== 'production') {
    templateMenu.push({
        label: 'Dev Tools',
        submenu: [
            {
                label: 'Show/Hide Dev Tools',
                click(item, focusedWindow) {
                    focusedWindow.toggleDevTools()
                }
            },
            {
                role: 'reload'
            }
        ]
    })
}

function guardarVisita(dni) {

    let nuevaVisita = new NuevaVisitaDB({
        nombre: dni.nombre,
        apellido: dni.apellido,
        dni: dni.dni
    })

    nuevaVisita.save((err, nuevaVisitaGuardada) => {

        if (err) {
            console.log('Ocurrio un error al guardar la visita');
            console.log(err.message);
        }

        else if (!nuevaVisitaGuardada) {
            console.log('No hay datos de la visita en la BD');
        }

        else {
            console.log('Visita guardada en BD');
        }
    })
}