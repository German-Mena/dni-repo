const { ipcRenderer } = require('electron')
const products = document.querySelector('#products')

ipcRenderer.on('nuevaVisita', (event, dni) => {

    console.log(dni);

    const nuevaVisita = `
    <div class="col-xs-4 p-2">
        <div class="card text-center">
            <div class="card-header">
                <h5 class="card-title">DNI: ${dni.dni}</h5>
            </div>
            <div class="card-body">
                Nombre:    
                <hr/>
                ${dni.apellido}, ${dni.nombre}
            </div>
            <div class="card-footer">
                <button class="btn btn-danger btn-sm">
                DELETE
                </button>
            </div>
        </div>
    </div>
    `

    //Esto es para enviarlo al HTML
    products.innerHTML += nuevaVisita

    const btns = document.querySelectorAll('.btn.btn-danger')
    btns.forEach(btn => {
        btn.addEventListener('click', event => {
            //Al hacer parentElement subo una categoria hasta llegar al padre
            event.target.parentElement.parentElement.parentElement.remove()
        })
    });
})

ipcRenderer.on('product: remove-all', (e) => {
    products.innerHTML = ''
})

