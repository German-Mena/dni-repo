const mongoose = require('mongoose');
let Schema = mongoose.Schema

let nuevaVisitaSchema = new Schema({
    nombre: { type: String },
    apellido: { type: String },
    dni: { type: String }
})

module.exports = mongoose.model('Nueva Visita', nuevaVisitaSchema)